package main

import (
	"net"
	"fmt"
	"os"
)

var (
	pass = "test1234"
	nick = "damikin-bot"
	user = "damikin-bot damikin.org damikin.org :damikin-bot"
	pool_size = 10
	debug = false
)

func main() {
	normal_operation()
}

func normal_operation() {
	conn, err := net.Dial("tcp", "162.228.75.88:6667")
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to connect: %v\n", err);
		return
	}

	input := make(chan string, 100)
	output := make(chan string, 100)
	done := make(chan int, 3)

	go receiver(conn, input, done)
	go sender(conn, output, done)

	if !handle_register(input, output) {
		done <-1
		done <-1
		return
	}

	for i := 0; i < pool_size; i++ {
		go handle_messages(input, output)
	}
	output <-fmt.Sprintf(":%v JOIN #main", nick)

	<-done
}

