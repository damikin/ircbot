package main

import (
	"fmt"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"io"
	"errors"
)

func cmd_makekey(optional string) ([]byte, error) {
	key := make([]byte, aes.BlockSize)
	if len(optional) == 0 {
		if _, err := io.ReadFull(rand.Reader, key); err != nil {
			fmt.Printf("cmd_makekey error: %v", err)
			return key, err
		}
	} else {
		for len(optional) < aes.BlockSize {
			optional += optional
		}
		for i := 0; i < aes.BlockSize; i++ {
			key[i] = byte(optional[i])
		}
	}

	return key, nil
}

func cmd_encrypt(msg string, key []byte) ([]byte, error) {
	if len(key) < aes.BlockSize || len(key) > aes.BlockSize {
		fmt.Printf("cmd_encrypt: key invald: %v/%v\n", len(key), aes.BlockSize)
		return nil, aes.KeySizeError(len(key))
	}

	msg_len := len(msg)
	missing := aes.BlockSize - (msg_len % aes.BlockSize)
	plaintext := make([]byte, msg_len + missing)
	for i := 0; i < msg_len; i++ {
		plaintext[i] = byte(msg[i])
	}
	for i := 0; i < missing; i++ {
		plaintext[msg_len + i] = byte(missing)
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		fmt.Printf("cmd_encrypt error: aes.NewCipher: %v\n", err)
		return nil, err
	}

	ciphertext := make([]byte, aes.BlockSize + len(plaintext))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		fmt.Printf("cmd_encrypt error: io.ReadFull: %v\n", err)
		return nil, err
	}

	mode := cipher.NewCBCEncrypter(block, iv)
	mode.CryptBlocks(ciphertext[aes.BlockSize:], plaintext)

	return ciphertext, nil
}

func cmd_decrypt(ciphertext, key []byte) (string, error) {
	if len(key) < aes.BlockSize || len(key) > aes.BlockSize {
		fmt.Printf("cmd_decrypt: key invald: %v/%v\n", len(key), aes.BlockSize)
		return "", aes.KeySizeError(len(key))
	}

	if len(ciphertext) < aes.BlockSize {
		fmt.Printf("cmd_decrypt: ciphertext too short: %v/%v\n", len(ciphertext), aes.BlockSize)
		return "", errors.New("ciphertext too short")
	}
	if len(ciphertext) % aes.BlockSize != 0 {
		fmt.Printf("cmd_decrypt: ciphertext not multiple of block size: %v %% %v == %v\n", len(ciphertext), aes.BlockSize, len(ciphertext) % aes.BlockSize)
		return "", errors.New("ciphertext not multiple of block size")
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		fmt.Printf("cmd_decrypt error: aes.NewCipher: %v\n", err)
		return "", err
	}

	iv := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]
	mode := cipher.NewCBCDecrypter(block, iv)
	mode.CryptBlocks(ciphertext, ciphertext)

	ending := len(ciphertext) - int(ciphertext[len(ciphertext) - 1])
	return fmt.Sprintf("%s", ciphertext[:ending]), nil
}

