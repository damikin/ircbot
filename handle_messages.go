package main

import (
	"fmt"
	"strings"
	"regexp"
	"encoding/hex"
)

func handle_messages(input, output chan string) {
	var admin_nick = regexp.MustCompile(`^damikin11$`)
	var quit_msg = regexp.MustCompile(`^(?i:bot quit)$`)
	var greeting = regexp.MustCompile(`(?i:hello),?`)
	var encrypt = regexp.MustCompile(`^(?i:encrypt: )`)
	var decrypt = regexp.MustCompile(`^(?i:decrypt: )`)
	var python = regexp.MustCompile(`^(?i:python: )`)

	for {
		msg := <-input
		from, command, other := split_input(msg)

		switch strings.ToLower(command) {
		case "ping":
			if !debug {
				fmt.Printf("%v %v\n", command, other)
			}
			output <-fmt.Sprintf("PONG %v", other)

		case "privmsg":
			channel, privmsg := split_privmsg(other)
			if !debug {
				fmt.Printf("%v: %v: %v\n", channel, from, privmsg)
			}

			if strings.Compare(channel, nick) == 0 {
				channel = from
			}

			if quit_msg.MatchString(privmsg) && admin_nick.MatchString(from) {
				output <-make_privmsg(nick, channel, "Goodbye")
				output <-fmt.Sprintf(":%v QUIT", nick)
			}

			if greeting.MatchString(privmsg) {
				output <-make_privmsg(nick, channel, fmt.Sprintf("Hello %v!", from))
			}

			if encrypt.MatchString(privmsg) {
				encrypt_msg := strings.SplitN(privmsg, ": ", 2)[1]
				if len(encrypt_msg) == 0 {
					output <-make_privmsg(nick, channel, "help: encrypt: text to encrypt")
					break
				}

				key, err := cmd_makekey(nick)
				if err != nil {
					break
				}

				to_send, err := cmd_encrypt(encrypt_msg, key)
				if err != nil {
					break
				}

				output <-make_privmsg(nick, channel, fmt.Sprintf("%x", to_send))
			}

			if decrypt.MatchString(privmsg) {
				ciphertext := strings.SplitN(privmsg, ": ", 2)[1]
				if len(ciphertext) == 0 {
					output <-make_privmsg(nick, channel, "help: decrypt: ciphertext")
					break
				}

				key, err := cmd_makekey(nick)
				if err != nil {
					break
				}

				c_str, err := hex.DecodeString(ciphertext)
				if err != nil {
					fmt.Printf("hex.DecodeString error: %v\n", err)
					break
				}

				to_send, err := cmd_decrypt(c_str, key)
				if err != nil {
					break
				}

				output <-make_privmsg(nick, channel, to_send)
			}

			if python.MatchString(privmsg) {
				cmd := strings.SplitN(privmsg, ": ", 2)[1]
				if strings.Contains(cmd, "import") {
					to_send := "I currently don't allow imports!"
					output <-make_privmsg(nick, channel, to_send)
					break
				}

				to_send, err := run_python(cmd)
				if err != nil {
					to_send = err.Error()
				}

				parts := strings.Split(to_send, "\n")
				size := len(parts)
				if size > 5 {
					parts[5] = "..."
					size = 6
				}
				for i := 0; i < size; i++ {
					if parts[i] != "" {
						output <-make_privmsg(nick, channel, fmt.Sprintf("python: %v", parts[i]))
					}
				}
			}
		}
	}
}

