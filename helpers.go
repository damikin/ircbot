package main

import (
	"fmt"
	"strings"
	"regexp"
)

func make_privmsg(nick, channel, msg string) string {
	return fmt.Sprintf(":%v PRIVMSG %v :%v", nick, channel, msg)
}

func split_privmsg(other string) (string, string) {
	parts := strings.SplitN(other, ":", 2)
	channel := strings.TrimSpace(parts[0])
	msg := strings.TrimSpace(parts[1])
	return channel, msg
}

func split_input(msg string) (string, string, string) {
	var from, command, other string
	tmsg := strings.TrimSpace(msg)
	parts := strings.SplitN(tmsg, " ", 3)
	who := regexp.MustCompile(`:.*!`)

	if(parts[0][0] == ':') {
		from = strings.TrimSpace(parts[0])
		tfrom := who.FindString(from)
		if tfrom != "" {
			from = tfrom[1:len(tfrom)-1]
		}
		command = strings.TrimSpace(parts[1])
		other = strings.TrimSpace(parts[2])
	} else {
		command = strings.TrimSpace(parts[0])
		other = strings.TrimSpace(parts[1])
	}

	return from, command, other
}

