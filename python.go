package main

import (
	"fmt"
	"os/exec"
)

func run_python(str string) (string, error) {
	if str == "" {
		return "", nil
	}

	cmd := fmt.Sprintf("%v", str)
	out, err := exec.Command("/usr/local/bin/python", "-c", cmd).Output()
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%s", out), nil
}

