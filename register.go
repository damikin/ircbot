package main

import (
	"fmt"
	"strings"
)

func handle_register(input, output chan string) bool {
	notice_cnt := 0
	sent_register := false
	is_registered := false

	for !is_registered {
		msg := <-input
		_, command, other := split_input(msg)

		switch strings.ToLower(command) {
		case "ping":
			output <-fmt.Sprintf("PONG %v", other)
			is_registered = true
		case "notice":
			notice_cnt++
			if !sent_register && notice_cnt >= 2{
				sent_register = true
				output <-fmt.Sprintf("PASS %v", pass)
				output <-fmt.Sprintf("nick %v", nick)
				output <-fmt.Sprintf("user %v", user)
			}
		}
	}

	return true
}

