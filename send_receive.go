package main

import (
	"net"
	"fmt"
	"os"
	"bufio"
	"strings"
)

func receiver(conn net.Conn, input chan string, done chan int) {
	reader := bufio.NewReader(conn)
	for {
		msg, err := reader.ReadString('\n')
		if err != nil {
			if !strings.Contains(err.Error(), "EOF") {
				fmt.Fprintf(os.Stderr, "receiver error: %v: %v\n", err, msg)
			}
			done <-1
			done <-1
			return
		}

		select {
		case <-done:
			return
		case input <-msg:
			if debug {
				fmt.Printf("input:  %v", msg)
			}
		}
	}
}

func sender(conn net.Conn, output chan string, done chan int) {
	for {
		select {
		case <-done:
			return
		case msg := <-output:
			if debug {
				fmt.Printf("output: %v\n", msg)
			}
			fmt.Fprintf(conn, "%v\r\n", msg)
		}
	}
}

